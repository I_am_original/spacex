package com.raccoon.spacex

import androidx.recyclerview.widget.DiffUtil
import com.raccoon.spacex.state.LaunchItemUI
import com.raccoon.spacex.state.LoadingItem
import com.raccoon.spacex.state.PlaceholderItem
import com.raccoon.spacex.state.StateListItem

class DiffCallback : DiffUtil.Callback() {

    lateinit var oldItems: List<StateListItem>
    lateinit var newItems: List<StateListItem>

    override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean =
        when (val newItem = newItems[newPosition]) {
            is LaunchItemUI -> compareLaunchItems(newItem, oldItems[oldPosition])
            is LoadingItem -> oldItems[oldPosition] is LoadingItem
            is PlaceholderItem -> oldItems[oldPosition] is PlaceholderItem
        }

    private fun compareLaunchItems(newItem: StateListItem, oldItem: StateListItem): Boolean {
        oldItem as LaunchItemUI
        newItem as LaunchItemUI
        return newItem.missionId?.equals(oldItem.missionId) ?: false
    }

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean =
        oldItems[oldPosition] == newItems[newPosition]
}