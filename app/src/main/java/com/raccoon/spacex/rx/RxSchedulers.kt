package com.raccoon.spacex.rx

import com.raccoon.spacex.domain.rx.SchedulersFacade
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RxSchedulers : SchedulersFacade {

    override val io: Scheduler = Schedulers.io()

    override val main: Scheduler = AndroidSchedulers.mainThread()

    override val computation: Scheduler = Schedulers.computation()

}