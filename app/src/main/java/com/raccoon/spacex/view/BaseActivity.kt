package com.raccoon.spacex.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.LENGTH_SHORT
import com.raccoon.spacex.R
import com.raccoon.spacex.di.view_model.ViewModelFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

/**
 * Base activity with base functionality and base DI stuff
 * No sense to use DaggerAppCompatActivity - because of a lot of useless dependencies
 */
abstract class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    // reference to layout resource, should be defined in each activity
    abstract val layoutResource: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(layoutResource)
        findViewById<Toolbar>(R.id.toolbar)?.let { setUpToolbar(it) }
    }

    abstract fun setUpToolbar(toolbar: Toolbar)

    open fun showError(throwable: Throwable) {
        Snackbar
            .make(findViewById(android.R.id.content), throwable.localizedMessage, LENGTH_SHORT)
            .show()
    }
}