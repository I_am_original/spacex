package com.raccoon.spacex.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.raccoon.spacex.DiffCallback
import com.raccoon.spacex.state.LaunchItemUI
import com.raccoon.spacex.state.LoadingItem
import com.raccoon.spacex.state.PlaceholderItem
import com.raccoon.spacex.state.StateListItem
import com.raccoon.spacex.view.holder.LaunchViewHolder
import com.raccoon.spacex.view.holder.LoadingViewHolder
import com.raccoon.spacex.view.holder.PlaceholderViewHolder

typealias  OnItemClickListener = (LaunchItemUI, View) -> Unit

class LaunchesRecyclerAdapter(
        private val onClickListener: OnItemClickListener,
        private val applicationContext: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_LAUNCH_ITEM = 0
        const val TYPE_LOADING = 1
        const val TYPE_PLACEHOLDER = 2
        const val TYPE_EMPTY_ITEM = 3
    }

    override fun onCreateViewHolder(container: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            when (viewType) {
                TYPE_LAUNCH_ITEM -> {
                    LaunchViewHolder(
                            LayoutInflater.from(container.context).inflate(
                                    com.raccoon.spacex.R.layout.list_item_launch,
                                    container,
                                    false
                            )
                    )
                }
                TYPE_LOADING -> LoadingViewHolder(
                        LayoutInflater.from(container.context).inflate(
                                com.raccoon.spacex.R.layout.list_item_loading,
                                container,
                                false
                        )
                )
                TYPE_EMPTY_ITEM -> LoadingViewHolder(
                        LayoutInflater.from(container.context).inflate(
                                com.raccoon.spacex.R.layout.list_item_empty,
                                container,
                                false
                        )
                )
                else -> PlaceholderViewHolder(
                        LayoutInflater.from(container.context).inflate(
                                com.raccoon.spacex.R.layout.list_item_placeholder,
                                container,
                                false
                        )
                )
            }

    private val items: MutableList<StateListItem> = ArrayList()

    override fun getItemCount() =
            if (items.size == 0) {
                1
            } else {
                items.size
            }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (items.size > 0) {
            val item = items[holder.adapterPosition]
            when (getItemViewType(holder.adapterPosition)) {
                TYPE_LAUNCH_ITEM -> (holder as LaunchViewHolder).bindLaunchItem(applicationContext, item, onClickListener)
            }
        }
    }

    override fun getItemViewType(position: Int): Int =
            if (items.size == 0) {
                TYPE_EMPTY_ITEM
            } else {
                when (items[position]) {
                    is LaunchItemUI -> TYPE_LAUNCH_ITEM
                    is LoadingItem -> TYPE_LOADING
                    is PlaceholderItem -> TYPE_PLACEHOLDER
                }
            }

    fun clearList() {
        items.clear()
        notifyDataSetChanged()
    }

    fun updateList(stringItemList: List<StateListItem>) {
        dispatchDiff {
            var previousSize = itemCount
            if (items.isNotEmpty()) {
                if (items.last() is LoadingItem) {
                    items.remove(items.last())
                    previousSize--
                }
            }
            items.addAll(stringItemList)
            notifyItemRangeChanged(previousSize, itemCount - 1)
        }
    }

    private fun dispatchDiff(function: () -> Unit) {
        val callback = DiffCallback()
        callback.oldItems = items
        function()
        callback.newItems = items
        DiffUtil.calculateDiff(callback).dispatchUpdatesTo(this)
    }
}