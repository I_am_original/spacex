package com.raccoon.spacex.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import androidx.recyclerview.widget.LinearLayoutManager
import com.raccoon.spacex.DataEvent
import com.raccoon.spacex.EndlessScrollListener
import com.raccoon.spacex.EventObserver
import com.raccoon.spacex.R
import com.raccoon.spacex.R.*
import com.raccoon.spacex.state.LaunchItemUI
import com.raccoon.spacex.state.StateListItem
import com.raccoon.spacex.state.UIState
import com.raccoon.spacex.view.adapters.LaunchesRecyclerAdapter
import com.raccoon.spacex.view_model.LaunchesViewModel
import com.raccoon.spacex.view_model.observe
import com.raccoon.spacex.view_model.withViewModel
import kotlinx.android.synthetic.main.activity_launches.*
import kotlinx.android.synthetic.main.progress_bar.*


class LaunchesActivity : BaseActivity() {

    companion object {
        const val STURTUP_INIT = "STURTUP_INIT"
        const val TBD_FLAG = "TBD_FLAG"
    }

    private lateinit var recyclerViewAdapter: LaunchesRecyclerAdapter
    private var tbdFlag: Boolean = false

    private val viewModel: LaunchesViewModel by lazy {
        return@lazy withViewModel<LaunchesViewModel>(viewModelFactory) {
            observe(uiState, this@LaunchesActivity::onUiState)
            observe(liveData(), this@LaunchesActivity::onDataEvent)
        }
    }

    override val layoutResource: Int
        get() = layout.activity_launches

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_filter, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.apply {
            val all = getString(string.filter_all)
            val tbd = getString(string.filter_tbd)
            findItem(id.filterTBD).title = if (tbdFlag) all else tbd
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            id.filterTBD -> {
                tbdFlag = !tbdFlag
                val all = getString(string.filter_all)
                val tbd = getString(string.filter_tbd)
                item.title = if (tbdFlag) all else tbd
                recyclerViewAdapter.clearList()
                viewModel.filterList(tbdFlag)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tbdFlag = savedInstanceState?.getBoolean(TBD_FLAG) ?: false
        startupSetup()
        viewModel.initialize(savedInstanceState?.getBoolean(STURTUP_INIT) ?: true)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.apply {
            putBoolean(STURTUP_INIT, false)
            putBoolean(TBD_FLAG, tbdFlag)
        }
        super.onSaveInstanceState(outState)
    }

    override fun setUpToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        toolbar.title = getString(string.app_name)
        val color =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                resources.getColor(color.colorPrimary, null)
            else
                resources.getColor(color.colorPrimary)
        toolbar.setBackgroundColor(color)
    }

    private fun startupSetup() {
        recyclerViewAdapter = LaunchesRecyclerAdapter(::showLaunchDetails, applicationContext)
        with(launches_list) {
            layoutManager = LinearLayoutManager(baseContext)
            adapter = recyclerViewAdapter
            addOnScrollListener(object : EndlessScrollListener() {
                override fun onPositionChange(position: Int) {
                    viewModel.onScrollPositionChange(position)
                }
            })
        }
        swipe_to_refresh.setOnRefreshListener {
            recyclerViewAdapter.clearList()
            viewModel.forceRefresh()
        }
    }

    private fun onDataEvent(dataEvent: DataEvent<List<StateListItem>>) = EventObserver(::loadItems).onChanged(dataEvent)

    private fun onUiState(uiState: UIState) {
        when (uiState) {
            is UIState.Init -> viewModel.configureInteraction(uiState.initialLaunch)
            is UIState.LoadingList -> showLoader()
            is UIState.Error -> showError(uiState.error)
//            is UIState.LoadItems -> loadItems(uiState.launches)
//            is UIState.PagingList -> showPaginationState()
        }
    }

    private fun loadItems(launchesLIst: List<StateListItem>) {
        hideLoader()
        Log.d("Paginator", "itemsList updated in adapter with size ${launchesLIst.size}")
        recyclerViewAdapter.updateList(launchesLIst)
    }

    override fun showError(throwable: Throwable) {
        progress_bar.setGone()
        launches_list.setGone()
        error_state_layout.setVisible()
        error_state_layout.setOnClickListener {
            error_state_layout.setGone()
            progress_bar.setVisible()
            viewModel.retry()
        }
        super.showError(throwable)
    }

    private fun showLoader() {
        progress_bar.setVisible()
        launches_list.setGone()
    }

    private fun hideLoader() {
        progress_bar.setGone()
        launches_list.setVisible()
        swipe_to_refresh.isRefreshing = false
    }

    // Item click with animated start of next activity
    private fun showLaunchDetails(listItem: LaunchItemUI, sharedElement: View) {
        val intent = Intent(this, LaunchDetailsActivity::class.java)
        val patchLink = listItem.links?.let { it.missionPatchSmall ?: it.missionPatch }
        intent.putExtra(LaunchDetailsActivity.launchData, listItem)
        intent.putExtra(LaunchDetailsActivity.patchUrl, patchLink)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                Pair(
                    sharedElement.findViewById(id.mission_patch),
                    LaunchDetailsActivity.viewNameHeaderImage
                )
            )
            ContextCompat.startActivity(this, intent, activityOptions.toBundle())
        } else {
            startActivity(intent)
        }
    }
}
