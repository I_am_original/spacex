package com.raccoon.spacex.view

import com.raccoon.spacex.DataEvent
import com.raccoon.spacex.domain.request.LaunchesPagingRequest
import com.raccoon.spacex.domain.usecase.GetLaunchesUseCase
import com.raccoon.spacex.pagination.Paginator
import com.raccoon.spacex.state.ItemUIMapper
import com.raccoon.spacex.state.LaunchItemUI
import com.raccoon.spacex.state.StateListItem
import io.reactivex.Observable
import io.reactivex.rxkotlin.zipWith
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class LaunchesInteractor @Inject constructor(
    private val getLaunchesUseCase: GetLaunchesUseCase
) : Paginator<StateListItem>() {

    private val cache = ArrayList<StateListItem>()
    private val retrySubject = PublishSubject.create<Boolean>()
    private lateinit var errorListener: (Throwable) -> Unit

    var isTBD: Boolean = false

    fun retry() = retrySubject.onNext(true)

    override fun loadData(limit: Int, offset: Int): Observable<DataEvent<List<StateListItem>>> {
        return getLaunchesUseCase.execute(LaunchesPagingRequest(limit, offset))
            .doOnError(errorListener)
            .retryWhen { it.zipWith(retrySubject) { t1, _ -> t1 } }
            .map(ItemUIMapper::mapToLaunchItemUIList)
    }

    fun forceRefresh() {
        dispose()
        loadInitial(totalItems)
    }

    fun pushCache() {
        liveData.postValue(DataEvent(filtered(cache)))
    }

    fun filterList(tbdFlag: Boolean) {
        isTBD = tbdFlag
        pushCache()
    }

    override fun postData(dataEvent: DataEvent<List<StateListItem>>) {
        cache.addAll(dataEvent.peekContent())
        liveData.postValue(DataEvent(filtered(dataEvent.peekContent())))
    }

    private fun filtered(dataList: List<StateListItem>): List<LaunchItemUI> {
        return dataList
            .map { item -> item as LaunchItemUI }
            .filter { item -> if (isTBD) item.tbd == isTBD else true }
    }

    override fun onEmptyFeed() {

    }

    override fun onLoadInitial() {
    }

    fun onErrorListener(listener: (Throwable) -> Unit) {
        errorListener = listener
    }
}