package com.raccoon.spacex.view

import android.os.Bundle
import android.util.SparseArray
import androidx.appcompat.widget.Toolbar
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.raccoon.spacex.R
import com.raccoon.spacex.state.LaunchItemUI
import com.raccoon.spacex.view.adapters.LaunchDetailsAdapter
import com.raccoon.spacex.view_model.LaunchDetailsViewModel
import com.raccoon.spacex.view_model.observe
import com.raccoon.spacex.view_model.withViewModel
import kotlinx.android.synthetic.main.activity_details.*

class LaunchDetailsActivity : BaseActivity() {

    companion object {
        const val launchData = "LAUNCH_DATA"
        const val patchUrl = "PATCH_DATA"
        const val viewNameHeaderImage = "detail:header:image"
    }

    private val viewModel: LaunchDetailsViewModel by lazy {
        return@lazy withViewModel<LaunchDetailsViewModel>(viewModelFactory) {
            observe(singleLaunchData, this@LaunchDetailsActivity::onData)
        }
    }

    private val detailsAdapter = LaunchDetailsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpRecyclerView()
        viewModel.requestData(getDataItem())
    }

    override var layoutResource = R.layout.activity_details

    override fun setUpToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        collapsing_toolbar.title = getDataItem()?.missionName
        Glide
                .with(applicationContext)
                .load(getPatch() ?: R.drawable.space_x)
                .onlyRetrieveFromCache(true)
                .placeholder(R.drawable.space_x)
                .into(toolbar_image)

        ViewCompat.setTransitionName(toolbar_image, viewNameHeaderImage)
    }

    private fun getDataItem(): LaunchItemUI? = intent.getParcelableExtra(launchData)
    private fun getPatch(): String? = intent.getStringExtra(patchUrl)

    private fun setUpRecyclerView() {
        with(info_recycler_view) {
            layoutManager = LinearLayoutManager(this@LaunchDetailsActivity)
            adapter = detailsAdapter
        }
    }

    private fun onData(itemData: SparseArray<String>) {
        detailsAdapter.updateData(itemData)
    }
}