package com.raccoon.spacex.view.holder

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.raccoon.spacex.R
import com.raccoon.spacex.state.LaunchItemUI
import com.raccoon.spacex.state.StateListItem
import com.raccoon.spacex.view.adapters.OnItemClickListener
import kotlinx.android.synthetic.main.list_item_launch_body.view.*
import java.text.SimpleDateFormat
import java.util.*

class LaunchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    @SuppressLint("SetTextI18n")
    fun bindLaunchItem(applicationContext: Context, item: StateListItem, onClickListener: OnItemClickListener) {
        with(item as LaunchItemUI) {
            item.links?.let {
                Glide
                    .with(applicationContext)
                    .load(item.links.missionPatchSmall ?: item.links.missionPatch ?: R.drawable.space_x)
                    .centerCrop()
                    .placeholder(R.drawable.space_x)
                    .into(itemView.mission_patch)
            }
            itemView.flight_number.text = "# $flightNumber"
            launchDateUnix?.let {
                itemView.launch_date.text = SimpleDateFormat(
                    "yyyy.MM.dd",
                    Locale.getDefault()
                ).format(Date(it.toLong()*1000))
            }

            missionName.let { itemView.mission_name.text = "Name: ${it ?: "Unknown"}" }
            missionId.let {
                itemView.mission_id.text =
                    "ID: ${if (it.isNullOrEmpty()) "Unknown" else it.toString()}"
            }
        }

        itemView.setOnClickListener { onClickListener(item, itemView) }
    }
}