package com.raccoon.spacex.view

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.graphics.Typeface
import com.google.android.material.tabs.TabLayout
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import android.text.ParcelableSpan
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.EditText
import android.widget.TextView
import java.util.regex.Pattern

fun View.setVisible() {
    this.visibility = View.VISIBLE
}

fun View.setGone() {
    this.visibility = View.GONE
}

fun View.toggle() {
    when (this@toggle.visibility) {
        View.GONE -> this@toggle.setVisible()
        View.VISIBLE -> this@toggle.setGone()
    }
}

fun View.forceToggle() {
    when (this@forceToggle.visibility) {
        View.GONE -> this@forceToggle.setVisible()
        View.VISIBLE -> Unit
    }
}

fun View.setGoneAnimate() {
    val view = this
    view.animate().setListener(object : AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator?) {
            view.setGone()
        }
    }).alpha(0f).start()
}

fun View.setVisibleAnimate() {
    val view = this
    view.animate().setListener(object : AnimatorListenerAdapter() {
        override fun onAnimationStart(animation: Animator?) {
            view.setVisible()
        }
    }).alpha(255f).start()
}

fun View.setInvisible() {
    this.visibility = View.INVISIBLE
}

fun EditText.string(): String {
    return this.text.toString()
}

fun TextView.string(): String {
    return this.text.toString()
}

fun TextView.addColorSpan(inputText: String?, color: Int) {
    val mask = "\\[.*?\\]"
    val sb = StringBuffer()
    val spannable = SpannableStringBuilder()

    val pattern = Pattern.compile(mask)
    val matcher = pattern.matcher(inputText)
    while (matcher.find()) {
        sb.setLength(0)
        val group = matcher.group()
        val spanText = group.substring(1, group.length - 1)
        matcher.appendReplacement(sb, spanText)

        spannable.append(sb)
        val start = spannable.length - spanText.length

        val spans = listOf<ParcelableSpan>(ForegroundColorSpan(color), StyleSpan(Typeface.BOLD))
        spans.forEach {
            spannable.setSpan(it, start, spannable.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
    sb.setLength(0)
    matcher.appendTail(sb)
    spannable.append(sb)
    this.text = spannable
}

fun View.animateView(propertyName: String, value: Float, duration: Long) {
    val animation = ObjectAnimator.ofFloat(this,
            propertyName,
            value)
    animation.duration = duration
    animation.interpolator = FastOutSlowInInterpolator()
    animation.start()
}

inline fun <T : View> T.afterMeasured(crossinline function: T.() -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (measuredWidth > 0 && measuredHeight > 0) {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                function()
            }
        }
    })
}

fun TabLayout.shortUnderlineIndicator(externalMargin: Int, internalMargin: Int) {
    val tabStrip = getChildAt(0)
    if (tabStrip is ViewGroup) {
        val childCount = tabStrip.childCount
        for (i in 0 until childCount) {
            val tabView = tabStrip.getChildAt(i)
            tabView.minimumWidth = 0
            tabView.setPadding(0, tabView.paddingTop, 0, tabView.paddingBottom)
            if (tabView.layoutParams is ViewGroup.MarginLayoutParams) {
                val layoutParams = tabView.layoutParams as ViewGroup.MarginLayoutParams
                when (i) {
                    0 -> settingMargin(layoutParams, externalMargin, internalMargin)
                    childCount - 1 -> settingMargin(layoutParams, internalMargin, externalMargin)
                    else -> settingMargin(layoutParams, internalMargin, internalMargin)
                }
            }
        }
        requestLayout()
    }
}

private fun settingMargin(layoutParams: ViewGroup.MarginLayoutParams, start: Int, end: Int) {
    layoutParams.marginStart = start
    layoutParams.marginEnd = end
    layoutParams.leftMargin = start
    layoutParams.rightMargin = end
}