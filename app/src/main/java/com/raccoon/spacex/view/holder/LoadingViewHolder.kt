package com.raccoon.spacex.view.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)