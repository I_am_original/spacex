package com.raccoon.spacex.view.adapters

import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.raccoon.spacex.R

/**
 * Adapter for Recycler View with information about selected by flag country
 */
class LaunchDetailsAdapter : RecyclerView.Adapter<LaunchDetailsAdapter.ItemViewHolder>() {

    private var items: SparseArray<String>? = null

    fun updateData(data: SparseArray<String>) {
        items = data
        notifyDataSetChanged()
    }

    override fun getItemCount() = items?.size() ?: 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.launch_details_item_info, parent, false))

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        items?.let {
            holder.title.setText(it.keyAt(position))
            holder.description.text = it.valueAt(position)
        }
    }

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.findViewById(R.id.text_view_title)
        var description: TextView = view.findViewById(R.id.text_view_description)
    }
}