package com.raccoon.spacex.view.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class PlaceholderViewHolder(view: View) : RecyclerView.ViewHolder(view)
