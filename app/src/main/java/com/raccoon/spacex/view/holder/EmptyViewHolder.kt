package com.raccoon.spacex.view.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class EmptyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)