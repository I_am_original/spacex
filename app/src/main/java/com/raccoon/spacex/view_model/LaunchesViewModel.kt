package com.raccoon.spacex.view_model

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.raccoon.spacex.state.UIState
import com.raccoon.spacex.view.LaunchesInteractor
import javax.inject.Inject

class LaunchesViewModel @Inject constructor(
    private val launchesInteractor: LaunchesInteractor
) : ViewModel() {

    val uiState = MutableLiveData<UIState>()

    fun initialize(initialLaunch: Boolean) = uiState.postValue(UIState.Init(initialLaunch))

    fun configureInteraction(initialLaunch: Boolean) {
        if (initialLaunch) {
            launchesInteractor.onErrorListener {
                Log.e(LaunchesViewModel::class.java.simpleName, "Launches ${it.localizedMessage}")
                uiState.postValue(UIState.Error(it))
            }
            uiState.postValue(UIState.LoadingList)
            launchesInteractor.loadInitial()
        } else {
            uiState.postValue(UIState.LoadingList)
            launchesInteractor.pushCache()
        }
    }


    fun liveData() = launchesInteractor.liveData

    fun retry() = launchesInteractor.retry()

    fun forceRefresh() = launchesInteractor.forceRefresh()

    fun filterList(tbdFlag: Boolean) = launchesInteractor.filterList(tbdFlag)

    fun onScrollPositionChange(position: Int) = launchesInteractor.onPositionChange(position)
}