package com.raccoon.spacex.view_model

import android.util.SparseArray
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.raccoon.spacex.state.LaunchItemUI
import javax.inject.Inject

class LaunchDetailsViewModel @Inject constructor() : ViewModel() {

    val singleLaunchData = MutableLiveData<SparseArray<String>>()

    fun requestData(launch: LaunchItemUI?) =
        singleLaunchData.postValue(launch?.toMapOfData())
}