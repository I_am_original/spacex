package com.raccoon.spacex.pagination

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.raccoon.spacex.DataEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlin.math.min

abstract class Paginator<T> {

    private lateinit var disposable: Disposable
    private val pagesPaginator = PublishSubject.create<Int>()

    private val paginationDelta = 5
    private val itemsLimit = 15
    var totalItems = 0
    private var page = 0
    private var offset = 0

    val liveData = MutableLiveData<DataEvent<List<T>>>()
    val loadedPages: List<Int> = ArrayList()

    protected abstract fun postData(dataEvent: DataEvent<List<T>>)

    protected abstract fun onLoadInitial()
    protected abstract fun onEmptyFeed()
    protected abstract fun loadData(limit: Int = itemsLimit, offset: Int = 0): Observable<DataEvent<List<T>>>

    private fun loadNext() = pagesPaginator.onNext(page)

    fun loadInitial(limit: Int = itemsLimit, offset: Int = 0) {
        page = (limit - itemsLimit) / itemsLimit
        onLoadInitial()
        disposable =
                loadData(limit, offset)
                        .doOnNext {
                            if (it.peekContent().isNullOrEmpty()) {
                                onEmptyFeed()
                            } else {
                                page++
                                totalItems = it.peekContent().size
                                postData(it)
                            }
                        }
                        .switchMap { paginate() }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({}, { Log.d("PAGING_ERROR", it.toString()) })
    }

    fun onPositionChange(position: Int) {
        if (totalItems - position <= paginationDelta) {
            pagesPaginator.onNext(page)
        }
    }

    private fun paginate(): Observable<DataEvent<List<T>>> {
        return pagesPaginator
                .distinctUntilChanged()
                .filter { !(page > 0 && totalItems < itemsLimit) }
                .concatMap(::loadNextPage)
                .doOnNext {
                    page++
                    totalItems += it.peekContent().size
                    postData(it)
                }
    }

    private fun loadNextPage(page: Int): Observable<DataEvent<List<T>>> =
            loadData(offset = min(page * itemsLimit, totalItems))

    fun dispose() = disposable.dispose()
}
