package com.raccoon.spacex.pagination

import androidx.recyclerview.widget.DiffUtil

abstract class PagingDiffUtil<T>: DiffUtil.Callback() {

    abstract var oldItems: List<T>
    abstract var newItems: List<T>
}