package com.raccoon.spacex.pagination

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class BasePagingAdapter<T>(private val diffUtil: PagingDiffUtil<T>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    protected val items = ArrayList<T>()

    override fun getItemCount() = items.size

    protected fun dispatchDiff(function: () -> Unit) {
        diffUtil.oldItems = items
        function()
        diffUtil.newItems = items
        DiffUtil.calculateDiff(diffUtil).dispatchUpdatesTo(this)
    }
}
