package com.raccoon.spacex

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

abstract class EndlessScrollListener : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dx == 0 && dy == 0) return
        when (val layoutManager = recyclerView.layoutManager) {
            is LinearLayoutManager -> onPositionChange(layoutManager.findLastVisibleItemPosition())
            is GridLayoutManager -> onPositionChange(layoutManager.findLastVisibleItemPosition())
            is StaggeredGridLayoutManager -> {
                layoutManager.findLastVisibleItemPositions(null).apply {
                    if (isNotEmpty()) {
                        onPositionChange(this[0])
                    }
                }
            }
            else -> throw IllegalStateException("Endless scroll listener can be used by either LinearLayoutManager or GridLayoutManager")
        }
    }

    abstract fun onPositionChange(position: Int)
}