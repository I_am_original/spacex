package com.raccoon.spacex.state

import android.os.Parcelable
import android.util.SparseArray
import com.raccoon.spacex.R
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

sealed class StateListItem
object LoadingItem : StateListItem()
object PlaceholderItem : StateListItem()

@Parcelize
data class LaunchItemUI(

        val flightNumber: Int?,
        val missionName: String?,
        val missionId: List<String>?,
        val upcoming: Boolean?,
        val launchYear: Int?,
        val launchDateUnix: Int?,
        val launchDateUtc: String?,
        val launchDateLocal: String?,
        val isTentative: Boolean?,
        val tentativeMaxPrecision: String?,
        val tbd: Boolean?,
        val launchWindow: Int?,
        val rocket: RocketItemUI?,
        val ships: List<String>?,
        val launchSite: LaunchSiteItemUI?,
        val launchSuccess: Boolean?,
        val launchFailureDetails: LaunchFailureDetailsItemUI?,
        val links: LinksItemUI?,
        val details: String?,
        val staticFireDateUtc: String?,
        val staticFireDateUnix: Int?,
        val timeline: TimelineItemUI?
) : Parcelable, StateListItem() {

    fun toMapOfData(): SparseArray<String> {
        val resultMap: SparseArray<String> = SparseArray()
        resultMap.append(R.string.flightNumber, flightNumber.toString())
        resultMap.append(R.string.missionName, missionName ?: "Unknown")
        resultMap.append(R.string.missionId, if (missionId != null && missionId.isNotEmpty()) missionId[0] else "Unknown")
        resultMap.append(R.string.rocketId, rocket?.rocketId ?: "Unknown")
        resultMap.append(R.string.rocketName, rocket?.rocketName ?: "Unknown")
        resultMap.append(R.string.rocketType, rocket?.rocketType ?: "Unknown")
        resultMap.append(R.string.launchYear, launchYear?.toString() ?: "Unknown")
        resultMap.append(R.string.launchDate, launchDateUnix?.let {
            SimpleDateFormat("yyyy.MM.dd", Locale.getDefault())
                    .format(Date(it.toLong() * 1000))
        } ?: "Unknown")
        resultMap.append(R.string.launchSiteId, launchSite?.siteId ?: "Unknown")
        resultMap.append(R.string.launchSiteName, launchSite?.siteName ?: "Unknown")
        resultMap.append(R.string.launchSiteNameLong, launchSite?.siteNameLong ?: "Unknown")
        resultMap.append(R.string.details, details ?: "Unknown")
        return resultMap
    }
}

@Parcelize
data class RocketItemUI(

        val rocketId: String?,
        val rocketName: String?,
        val rocketType: String?
) : Parcelable

@Parcelize
data class LaunchSiteItemUI(

        val siteId: String?,
        val siteName: String?,
        val siteNameLong: String?
) : Parcelable

@Parcelize
data class LaunchFailureDetailsItemUI(

        val time: Int?,
        val altitude: String?,
        val reason: String?
) : Parcelable

@Parcelize
data class LinksItemUI(

        val missionPatch: String?,
        val missionPatchSmall: String?,
        val redditCampaign: String?,
        val redditLaunch: String?,
        val redditRecovery: String?,
        val redditMedia: String?,
        val presskit: String?,
        val articleLink: String?,
        val wikipedia: String?,
        val videoLink: String?,
        val youtubeId: String?,
        val flickrImages: List<String?>?
) : Parcelable

@Parcelize
data class TimelineItemUI(

        val webcastLiftoff: Int?
) : Parcelable
