package com.raccoon.spacex.state

import com.raccoon.spacex.DataEvent
import com.raccoon.spacex.domain.model.Launch

object ItemUIMapper {

    fun mapToLaunchItemUIList(list: List<Launch>): DataEvent<List<LaunchItemUI>> =
            DataEvent(list.map { mapToLaunchItemUI(it) })

    private fun mapToLaunchItemUI(launchData: Launch): LaunchItemUI =
            LaunchItemUI(
                    flightNumber = launchData.flightNumber,
                    missionName = launchData.missionName,
                    missionId = launchData.missionId,
                    upcoming = launchData.upcoming,
                    launchYear = launchData.launchYear,
                    launchDateUnix = launchData.launchDateUnix,
                    launchDateUtc = launchData.launchDateUtc,
                    launchDateLocal = launchData.launchDateLocal,
                    isTentative = launchData.isTentative,
                    tentativeMaxPrecision = launchData.tentativeMaxPrecision,
                    tbd = launchData.tbd,
                    launchWindow = launchData.launchWindow,
                    rocket = RocketItemUI(
                            rocketId = launchData.rocket?.rocketId,
                            rocketName = launchData.rocket?.rocketName,
                            rocketType = launchData.rocket?.rocketType
                    ),
                    ships = launchData.ships,
                    launchSite = LaunchSiteItemUI(
                            siteId = launchData.launchSite?.siteId,
                            siteName = launchData.launchSite?.siteName,
                            siteNameLong = launchData.launchSite?.siteNameLong
                    ),
                    launchSuccess = launchData.launchSuccess,
                    launchFailureDetails = LaunchFailureDetailsItemUI(
                            time = launchData.launchFailureDetails?.time,
                            altitude = launchData.launchFailureDetails?.altitude,
                            reason = launchData.launchFailureDetails?.reason
                    ),
                    links = LinksItemUI(
                            missionPatch = launchData.links?.missionPatch,
                            missionPatchSmall = launchData.links?.missionPatchSmall,
                            redditCampaign = launchData.links?.redditCampaign,
                            redditLaunch = launchData.links?.redditLaunch,
                            redditRecovery = launchData.links?.redditRecovery,
                            redditMedia = launchData.links?.redditMedia,
                            presskit = launchData.links?.presskit,
                            articleLink = launchData.links?.articleLink,
                            wikipedia = launchData.links?.wikipedia,
                            videoLink = launchData.links?.videoLink,
                            youtubeId = launchData.links?.youtubeId,
                            flickrImages = launchData.links?.flickrImages
                    ),
                    details = launchData.details,
                    staticFireDateUtc = launchData.staticFireDateUtc,
                    staticFireDateUnix = launchData.staticFireDateUnix,
                    timeline = TimelineItemUI(webcastLiftoff = launchData.timeline?.webcastLiftoff)
            )
}