package com.raccoon.spacex.state

sealed class UIState {
    data class Init(val initialLaunch: Boolean) : UIState()
    object LoadingList : UIState()
    object PagingList : UIState()
    data class Error(val error: Throwable) : UIState()
    data class LoadItems(val launches: List<StateListItem>) : UIState()
}