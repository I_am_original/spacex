package com.raccoon.spacex.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.raccoon.spacex.di.view_model.ViewModelFactory
import com.raccoon.spacex.di.view_model.ViewModelKey
import com.raccoon.spacex.view_model.LaunchDetailsViewModel
import com.raccoon.spacex.view_model.LaunchesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LaunchesViewModel::class)
    internal abstract fun provideLaunchesViewModel(viewModel: LaunchesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LaunchDetailsViewModel::class)
    internal abstract fun provideLaunchDetailsViewModel(viewModel: LaunchDetailsViewModel): ViewModel

}