package com.raccoon.spacex.di.module

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.raccoon.spacex.data.repository.LaunchesRepositoryImpl
import com.raccoon.spacex.data.rest.ApiFactory
import com.raccoon.spacex.data.rest.SpaceXApi
import com.raccoon.spacex.di.scope.PerApplication
import com.raccoon.spacex.domain.repository.LaunchesRepository
import com.raccoon.spacex.domain.rx.SchedulersFacade
import com.raccoon.spacex.rx.RxSchedulers
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

@Module
open class ApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context = application.applicationContext

    @Provides
    fun providesOkHttpClientBuilder(): OkHttpClient.Builder = OkHttpClient().newBuilder()

    @Provides
    @PerApplication
    fun provideGson(): Gson = GsonBuilder().enableComplexMapKeySerialization().create()

    @Provides
    @PerApplication
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient().newBuilder().build()

    @Provides
    @PerApplication
    fun provideSpaceXApi(okHttpClient: OkHttpClient, gson: Gson): SpaceXApi =
        ApiFactory().create(SpaceXApi::class.java, SpaceXApi.apiEndpoint, okHttpClient, gson)

    @Provides
    @PerApplication
    fun provideSchedulers(): SchedulersFacade = RxSchedulers()

    @Provides
    @PerApplication
    fun provideLaunchesRepository(spaceXApi: SpaceXApi): LaunchesRepository =
        LaunchesRepositoryImpl(spaceXApi)
}