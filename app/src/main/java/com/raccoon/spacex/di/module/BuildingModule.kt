package com.raccoon.spacex.di.module

import com.raccoon.spacex.view.LaunchDetailsActivity
import com.raccoon.spacex.view.LaunchesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildingModule {

    @ContributesAndroidInjector
    abstract fun bindLaunchesActivity(): LaunchesActivity

    @ContributesAndroidInjector
    abstract fun bindDetailedViewActivity(): LaunchDetailsActivity

}