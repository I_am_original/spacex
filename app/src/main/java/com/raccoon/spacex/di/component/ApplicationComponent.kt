package com.raccoon.spacex.di.component

import com.raccoon.spacex.SpaceXApp
import com.raccoon.spacex.di.module.ApplicationModule
import com.raccoon.spacex.di.module.BuildingModule
import com.raccoon.spacex.di.module.ViewModelModule
import com.raccoon.spacex.di.scope.PerApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@PerApplication
@Component(modules = [(AndroidSupportInjectionModule::class), (BuildingModule::class), (ApplicationModule::class), (ViewModelModule::class)])
interface ApplicationComponent : AndroidInjector<SpaceXApp> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<SpaceXApp>()
}