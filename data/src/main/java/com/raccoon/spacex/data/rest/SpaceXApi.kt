package com.raccoon.spacex.data.rest

import com.raccoon.spacex.domain.model.Launch
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface SpaceXApi {

    companion object {
        const val apiEndpoint = "https://api.spacexdata.com/v3/"
    }

    @GET("launches")
    fun launchesPaged(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Observable<List<Launch>>

}