package com.raccoon.spacex.data.repository

import com.raccoon.spacex.data.rest.SpaceXApi
import com.raccoon.spacex.domain.model.Launch
import com.raccoon.spacex.domain.repository.LaunchesRepository
import com.raccoon.spacex.domain.request.LaunchesPagingRequest
import io.reactivex.Observable

class LaunchesRepositoryImpl(private val spaceXApi: SpaceXApi) : LaunchesRepository {

    override fun getLaunches(args: LaunchesPagingRequest): Observable<List<Launch>> =
        spaceXApi.launchesPaged(args.limit, args.offset)

}