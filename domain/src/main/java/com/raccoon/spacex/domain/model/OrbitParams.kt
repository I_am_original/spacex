package com.raccoon.spacex.domain.model

import com.google.gson.annotations.SerializedName

data class OrbitParams(

    @SerializedName("reference_system") val referenceSystem: String?,
    @SerializedName("regime") val regime: String?,
    @SerializedName("longitude") val longitude: String?,
    @SerializedName("semi_major_axis_km") val semiMajorAxisKm: String?,
    @SerializedName("eccentricity") val eccentricity: String?,
    @SerializedName("periapsis_km") val periapsisKm: Float?,
    @SerializedName("apoapsis_km") val apoapsisKm: Float?,
    @SerializedName("inclination_deg") val inclinationDeg: Float?,
    @SerializedName("period_min") val periodMin: String?,
    @SerializedName("lifespan_years") val lifespanYears: String?,
    @SerializedName("epoch") val epoch: String?,
    @SerializedName("mean_motion") val meanMotion: String?,
    @SerializedName("raan") val raan: String?,
    @SerializedName("arg_of_pericenter") val argOfPericenter: String?,
    @SerializedName("mean_anomaly") val meanAnomaly: String?
)