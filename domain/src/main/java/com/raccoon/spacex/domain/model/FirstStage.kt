package com.raccoon.spacex.domain.model

import com.google.gson.annotations.SerializedName

data class FirstStage(

    @SerializedName("cores") val cores: List<Cores>?
)