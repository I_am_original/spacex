package com.raccoon.spacex.domain.model

import com.google.gson.annotations.SerializedName

data class Telemetry(

    @SerializedName("flight_club") val flightClub: String?
)