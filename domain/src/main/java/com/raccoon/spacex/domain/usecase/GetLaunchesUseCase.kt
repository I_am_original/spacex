package com.raccoon.spacex.domain.usecase

import com.raccoon.spacex.domain.model.Launch
import com.raccoon.spacex.domain.repository.LaunchesRepository
import com.raccoon.spacex.domain.request.LaunchesPagingRequest
import com.raccoon.spacex.domain.rx.SchedulersFacade
import io.reactivex.Observable
import javax.inject.Inject

class GetLaunchesUseCase @Inject constructor(
    private val launchesRepository: LaunchesRepository,
    schedulers: SchedulersFacade
) : ObservableUseCase<LaunchesPagingRequest, List<Launch>>(schedulers) {

    override fun buildUseCase(args: LaunchesPagingRequest): Observable<List<Launch>> =
        launchesRepository.getLaunches(args)
}