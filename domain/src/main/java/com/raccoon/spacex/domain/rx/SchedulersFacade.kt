package com.raccoon.spacex.domain.rx

import io.reactivex.Scheduler

interface SchedulersFacade {

    val io: Scheduler

    val main: Scheduler

    val computation: Scheduler
}