package com.raccoon.spacex.domain.model

import com.google.gson.annotations.SerializedName

data class LaunchFailureDetails(

    @SerializedName("time") val time: Int?,
    @SerializedName("altitude") val altitude: String?,
    @SerializedName("reason") val reason: String?
)