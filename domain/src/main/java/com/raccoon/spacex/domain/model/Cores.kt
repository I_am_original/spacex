package com.raccoon.spacex.domain.model

import com.google.gson.annotations.SerializedName

data class Cores(

    @SerializedName("core_serial") val coreSerial: String?,
    @SerializedName("flight") val flight: Int?,
    @SerializedName("block") val block: String?,
    @SerializedName("gridfins") val gridfins: Boolean?,
    @SerializedName("legs") val legs: Boolean?,
    @SerializedName("reused") val reused: Boolean?,
    @SerializedName("land_success") val landSuccess: String?,
    @SerializedName("landing_intent") val landingIntent: Boolean?,
    @SerializedName("landing_type") val landingType: String?,
    @SerializedName("landing_vehicle") val landingVehicle: String?
)