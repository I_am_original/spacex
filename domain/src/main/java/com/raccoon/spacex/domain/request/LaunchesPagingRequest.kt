package com.raccoon.spacex.domain.request

data class LaunchesPagingRequest(val limit: Int, val offset: Int)