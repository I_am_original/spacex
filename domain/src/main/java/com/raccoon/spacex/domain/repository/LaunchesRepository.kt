package com.raccoon.spacex.domain.repository

import com.raccoon.spacex.domain.model.Launch
import com.raccoon.spacex.domain.request.LaunchesPagingRequest
import io.reactivex.Observable

interface LaunchesRepository {

    fun getLaunches(args: LaunchesPagingRequest): Observable<List<Launch>>

}