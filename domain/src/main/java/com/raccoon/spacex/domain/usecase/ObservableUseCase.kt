package com.raccoon.spacex.domain.usecase

import com.raccoon.spacex.domain.rx.SchedulersFacade
import io.reactivex.Observable

abstract class ObservableUseCase<T, R>(private val schedulersFacade: SchedulersFacade) {

    abstract fun buildUseCase(args: T): Observable<R>

    fun execute(args: T): Observable<R> {
        return buildUseCase(args)
            .observeOn(schedulersFacade.main)
            .subscribeOn(schedulersFacade.io)
    }
}